package um.edu.uy.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import um.edu.uy.business.exceptions.ClientAlreadyExists;
import um.edu.uy.business.exceptions.InvalidClientInformation;

import static org.junit.Assert.fail;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ClientTest {

    @Autowired
    ClientMgr clientMgr;

    @Test
    public void testBasic() {

        try {

            // Se agrega un cliente

            clientMgr.addClient(123123l, "daniel","colonia 1234");

        } catch (InvalidClientInformation invalidClientInformation) {

            fail(invalidClientInformation.getMessage());

        } catch (ClientAlreadyExists clientAlreadyExists) {

            fail(clientAlreadyExists.getMessage());

        }


        try {

            // Se prueba agregar el mismo cliente con la cedula

            clientMgr.addClient(123123l, "daniel","colonia 1234");

            fail("Cliente ya existia");

        } catch (InvalidClientInformation invalidClientInformation) {
            invalidClientInformation.printStackTrace();
        } catch (ClientAlreadyExists clientAlreadyExists) {

            // Ok flujo correcto
        }
    }


}
