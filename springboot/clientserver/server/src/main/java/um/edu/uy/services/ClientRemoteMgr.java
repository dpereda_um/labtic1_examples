package um.edu.uy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.edu.uy.business.ClientMgr;
import um.edu.uy.commons.ClientRemoteMgt;
import um.edu.uy.commons.exceptions.ClientAlreadyExists;
import um.edu.uy.commons.exceptions.InvalidClientInformation;

@Service
public class ClientRemoteMgr implements ClientRemoteMgt {

    @Autowired
    private ClientMgr clientMgr;

    @Override
    public void addClient(long document, String name, String address)
            throws InvalidClientInformation, ClientAlreadyExists {
        clientMgr.addClient(document, name, address);
    }
}
