package um.edu.uy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import um.edu.uy.commons.ClientRemoteMgt;
import um.edu.uy.commons.RemoteConstants;

import javax.annotation.PostConstruct;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

@Service
public class RemoteFactory {

    @Autowired
    private ClientRemoteMgr clientRemoteMgr;

    @Value("${server.rmi.port}")
    private int remotePort;

    @PostConstruct
    public void init() throws RemoteException {

        // Create Registry an register remote

        Registry oRegistry = LocateRegistry.createRegistry(remotePort);

        // Export all remote objects
        ClientRemoteMgt oStub = (ClientRemoteMgt) UnicastRemoteObject.exportObject(clientRemoteMgr, 0);
        oRegistry.rebind(RemoteConstants.CLIENT_REMOTE_NAME, oStub);

    }
}
