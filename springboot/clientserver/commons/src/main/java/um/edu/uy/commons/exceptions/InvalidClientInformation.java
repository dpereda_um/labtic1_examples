package um.edu.uy.commons.exceptions;

public class InvalidClientInformation extends Exception {

    public InvalidClientInformation(String message) {
        super(message);
    }
}
