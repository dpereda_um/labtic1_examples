package um.edu.uy.commons;

import um.edu.uy.commons.exceptions.ClientAlreadyExists;
import um.edu.uy.commons.exceptions.InvalidClientInformation;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientRemoteMgt extends Remote {

    void addClient(long document, String name, String address)
            throws InvalidClientInformation, ClientAlreadyExists, RemoteException;
}
