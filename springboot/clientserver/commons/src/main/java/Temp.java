import java.math.BigDecimal;

public class Temp {

    public static void main(String[] args) {
        BigDecimal codigoUnico = new BigDecimal("123412345");

        System.out.println(new BigDecimal(codigoUnico.longValue() % 100000));
        System.out.println(new BigDecimal(codigoUnico.longValue() / 100000));

    }
}
