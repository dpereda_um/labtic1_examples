package um.edu.uy.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import um.edu.uy.commons.ClientRemoteMgt;
import um.edu.uy.commons.RemoteConstants;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

@Service
public class RemoteClientFactory {

    @Value("${client.rmi.port}")
    private int remotePort;


    public ClientRemoteMgt getClientRemoteMgt() throws RemoteException {
        ClientRemoteMgt clientRemoteMgt = null;
        Registry oRegistry = LocateRegistry.getRegistry(remotePort);

        try {

            clientRemoteMgt = (ClientRemoteMgt) oRegistry.lookup(RemoteConstants.CLIENT_REMOTE_NAME);

        } catch (NotBoundException e) {
            throw new RuntimeException("No se encuentra uno de los objetos remotos con el nombre indicado");
        }

        return clientRemoteMgt;
    }
}
